<?php
/**
 * Created by PhpStorm.
 * User: tianshupei
 * Date: 2019/7/2
 * Time: 13:46
 */

namespace Tsp\UpgradeManager;

use \PDO;


class Db
{

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Db constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }


    public function fetch($sql, $params = [])
    {
        $statement = $this->pdo->prepare($sql);
        $result = $statement->execute($params);

        if (!$result) {
            return [];
        } else {
            $data = $statement->fetch(\PDO::FETCH_ASSOC);
            return $data;
        }
    }

    public function fetchall($sql, $params = [], $keyfield = '')
    {
        $statement = $this->pdo->prepare($sql);
        $result = $statement->execute($params);

        if (!$result) {
            return false;
        } else {
            if (empty($keyfield)) {
                $result = $statement->fetchAll(pdo::FETCH_ASSOC);
            } else {
                $temp = $statement->fetchAll(pdo::FETCH_ASSOC);
                $result = [];
                if (!empty($temp)) {
                    foreach ($temp as $key => &$row) {
                        if (isset($row[$keyfield])) {
                            $result[$row[$keyfield]] = $row;
                        } else {
                            $result[] = $row;
                        }
                    }
                }
            }
            return $result;
        }
    }

    /**
     * @param $sql
     * @param array $params
     * @return bool|int
     */
    public function execute($sql, $params = [])
    {
        if (!$params) {
            return $this->pdo->exec($sql);
        }
        $statement = $this->pdo->prepare($sql);
        $result = $statement->execute($params);
        if (!$result) {
            return false;
        } else {
            return $statement->rowCount();
        }
    }
}