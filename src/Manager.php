<?php
/**
 * Created by PhpStorm.
 * User: tianshupei
 * Date: 2019/7/2
 * Time: 16:38
 */

namespace Tsp\UpgradeManager;


use GuzzleHttp\Client;

class Manager
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
        ]);
    }

    protected function envs($app_identifiler, $app_ver, $extra = [])
    {
        $os = strtolower(PHP_OS) === 'linux' ? 'linux' : 'win';
        $php_ver = PHP_VERSION;
        $enc_ver = phpversion('swoole_loader');

        return array_merge(compact(
            'app_identifiler',
            'app_ver',
            'php_ver',
            'enc_ver',
            'os'
        ), $extra);
    }

    /**
     * 获取更新信息
     */
    public function check($url, $app_identifiler, $app_ver, $extra = [])
    {
        $res = $this->client->get($url, ['query' => $this->envs($app_identifiler, $app_ver, $extra)])->getBody()->getContents();

        return json_decode($res, true);
    }

    /*
     * 下载更新文件
     */
    public function download($url, $dst)
    {
        $filePath = $dst . '/app.tmp';
        $this->client->get($url, ['save_to' => $filePath]);
        if (!file_exists($filePath)) {
            throw new \Exception('更新包下载失败');
        }

        $zip = new \ZipArchive();
        if (($errcode = $zip->open($filePath)) !== true) {
            throw new \Exception("解压文件失败,code: ${errcode}");
        }

        $zip->extractTo($dst);
    }

    public function upgrade($src, $dst, $pdo)
    {
        $this->updateFiles("{$src}/files", $dst);
        $this->updateSchemas("{$src}/upgrade.schema", $pdo);
        $this->updateScripts("{$src}/upgrade.php");
    }

    /**
     * 更新文件
     */
    public function updateFiles($src, $dst)
    {
        $this->copyDir($src, $dst);
    }

    protected function copyDir($src, $dst)
    {
        if (is_file($dst)) return;

        if (!file_exists($dst)) {
            mkdir($dst, 0755);
        }

        if (!($handler = opendir($src))) return;

        while ($file = readdir($handler)) {
            if ($file !== '.' && $file !== '..') {
                $srcFile = "${src}/${file}";
                $dstFile = "${dst}/${file}";
                if (is_dir($srcFile)) {
                    $this->copyDir($srcFile, $dstFile);
                } else {
                    copy($srcFile, $dstFile);
                    chmod($dstFile,0755);
                }
            }

        }

        closedir($handler);
    }

    /**
     * 更新表结构
     */
    protected function updateSchemas($src, $pdo)
    {
        if (!file_exists($src)) return;
        $remotes = unserialize(file_get_contents($src));

        $db = new Db($pdo);
        $schema = new Schema($db);

        $sqls = [];
        foreach ($remotes as $remote) {
            $tablename = $remote['tablename'];
            $local = $schema->tableSchema($tablename);

            $sqls = array_merge($sqls, $schema->buildMigrateSql($local, $remote));
        }


        $schema->runMigrate($sqls);
    }

    /**
     * 执行更新脚本
     */
    protected function updateScripts($src)
    {
        if (file_exists($src) && is_file($src)) {
            require $src;
        }
    }
}
